\documentclass{article}

\title{A Very Brief Introduction to R}
\author{Sean Davis}
\date{June 21, 2013}

\begin{document}

\maketitle

\section{Vectors}

<<>>=
x = 1
x
y = rep('a',10)
y
z = rnorm(15)
z
z<0
z[z<0]
@ 


\section{Matrices}

<<>>=
m = matrix(rnorm(100),ncol=5)
ncol(m)
nrow(m)
dim(m)
summary(m)
# first column of m
m[,1]
# first row of m
m[1,]
# get a big matrix from R
data(volcano)
dim(volcano)
@ 

\section{Factors}

Remember that factors are used for storing categorical variables.  They often look like character vectors, but they do not have ``quotes'' when R prints them.  Also, they behave more like numeric vectors.

<<>>=
cvec = rep(c('a','b'),each=10)
cvec
f = factor(cvec)
f
levels(f)
# we can change the levels of the factor
levels(f) = c('d','e')
f
as.character(f)
@ 

\section{Data Frames}

<<>>=
df = data.frame(m,f)
dim(df)
head(df)
colnames(df)
df$X1
summary(df)
@ 

\section{Plotting}

This is where your curiosity and experimentation come in handy.  R has many plotting capabilities and some trial and error may be necessary to get what you want from R graphics.

<<fig=true>>=
plot(df[,1],df[,2],col=df$f,pch=2,main='A simple plot')
@ 

\section{Package installation}

<<eval=FALSE>>=
source('http://bioconductor.org/biocLite.R')
library(BiocInstaller)
biocLite(c('genefilter','qvalue'))
@ 

\subsection{Getting Help on a Package}

<<eval=FALSE>>=
help(package='genefilter')
@ 


\end{document}
